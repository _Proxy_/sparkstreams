package org.example;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.DataTypes;

/**
 * The DataEnrichment class is designed to enrich receipt data with weather data
 * and perform aggregation to find the most popular order type for each franchise
 * by date. It outputs the aggregated data to a specified location.
 */

public class DataEnrichment {

     /**
     * The main method that executes the data enrichment process.
     * 
     * @param args Command line arguments (unused).
     */
    public static void main(String[] args) {
        SparkSession spark = SparkSession.builder().appName("DataEnrichment").master("local[*]").getOrCreate();

        // Define paths
        String pathToReceipts = "src/main/resources/restaurants";
        String pathToWeather = "src/main/resources/weather";
        String pathToOutput = "output";

        // Read and preprocess the receipts data
        Dataset<Row> receiptsDf = spark.read().format("csv").option("header", "true").load(pathToReceipts)
                .withColumn("date", to_date(col("date_time")))
                .withColumn("lat", round(col("lat").cast(DataTypes.DoubleType), 2))
                .withColumn("lng", round(col("lng").cast(DataTypes.DoubleType), 2))
                .filter(year(col("date_time")).equalTo(lit(2022)));

        // Read and preprocess the weather data
        Dataset<Row> weatherDf = spark.read().format("csv").option("header", "true").load(pathToWeather)
                .withColumn("date", to_date(col("wthr_date")))
                .withColumn("lat", round(col("lat").cast(DataTypes.DoubleType), 2))
                .withColumn("lng", round(col("lng").cast(DataTypes.DoubleType), 2))
                .filter(year(col("wthr_date")).equalTo(lit(2022)));

        // Enrich receipts data with weather data
        Dataset<Row> enrichedDf = receiptsDf.join(weatherDf, receiptsDf.col("date").equalTo(weatherDf.col("date"))
                .and(receiptsDf.col("lat").equalTo(weatherDf.col("lat")))
                .and(receiptsDf.col("lng").equalTo(weatherDf.col("lng"))), "inner");

        // Filter by average temperature
        Dataset<Row> filteredDf = enrichedDf.filter(col("avg_tmpr_c").gt(0));

        // Calculate real total cost and order size
        Dataset<Row> resultDf = filteredDf.withColumn("real_total_cost", col("total_cost").minus(col("total_cost").multiply(col("discount"))))
                .withColumn("order_size", functions.when(col("items_count").isNull().or(col("items_count").leq(0)), "Erroneous")
                        .when(col("items_count").leq(1), "Tiny")
                        .when(col("items_count").leq(3), "Small")
                        .when(col("items_count").leq(10), "Medium")
                        .otherwise("Large"));

        // Define window specification for aggregating over date and franchise_id
        WindowSpec windowSpec =Window.partitionBy(col("date"), col("franchise_id"));

        // Add most_popular_order_type column
        Column mostPopularOrderType = when(col("Large_orders").equalTo(max(col("Large_orders")).over(windowSpec)), "Large")
                .when(col("Medium_orders").equalTo(max(col("Medium_orders")).over(windowSpec)), "Medium")
                .when(col("Small_orders").equalTo(max(col("Small_orders")).over(windowSpec)), "Small")
                .when(col("Tiny_orders").equalTo(max(col("Tiny_orders")).over(windowSpec)), "Tiny")
                .otherwise("Erroneous");

        // Compute most popular order type and write results
        Dataset<Row> finalDf = resultDf.withColumn("most_popular_order_type", mostPopularOrderType)
                .groupBy(col("date"), col("franchise_id"))
                .agg(count(when(col("order_size").equalTo("Erroneous"), true)).alias("erroneous_orders"),
                        count(when(col("order_size").equalTo("Tiny"), true)).alias("tiny_orders"),
                        count(when(col("order_size").equalTo("Small"), true)).alias("small_orders"),
                        count(when(col("order_size").equalTo("Medium"), true)).alias("medium_orders"),
                        count(when(col("order_size").equalTo("Large"), true)).alias("large_orders"),
                        max(col("most_popular_order_type")).alias("most_popular_order_type"));

        // Write the result to the specified path
        finalDf.write().option("header", "true").csv(pathToOutput);

        // Stop the Spark session
        spark.stop();
    }
}