package test;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;

import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.*;

/**
 * This class contains unit tests for the DataEnrichment application.
 * It tests the data loading, preprocessing, and enrichment steps of the application.
 * Each test ensures that the data transformations and joins are performed correctly and result in the expected schema and data content.
 */
public class DataEnrichmentTest {

    private static Dataset<Row> receiptsDf;
    private static Dataset<Row> weatherDf;
    private static Dataset<Row> enrichedDf;

    /**
     * Initializes Spark session and loads data from directories before all tests are run.
     * This setup handles multiple CSV files in each directory, processing them as part of a single DataFrame.
     */
    @BeforeClass
    public static void setUpClass() {
        SparkSession spark = SparkSession.builder()
                .appName("DataEnrichmentTest")
                .master("local[*]")
                .getOrCreate();

        String pathToReceipts = "src/test/resources/restaurants";
        String pathToWeather = "src/test/resources/weather";

        receiptsDf = spark.read().format("csv").option("header", "true").load(pathToReceipts)
                .withColumn("date", to_date(col("date_time")))
                .withColumn("lat", round(col("lat").cast(DataTypes.DoubleType), 2))
                .withColumn("lng", round(col("lng").cast(DataTypes.DoubleType), 2))
                .filter(year(col("date_time")).equalTo(lit(2022)));

        weatherDf = spark.read().format("csv").option("header", "true").load(pathToWeather)
                .withColumn("date", to_date(col("wthr_date")))
                .withColumn("lat", round(col("lat").cast(DataTypes.DoubleType), 2))
                .withColumn("lng", round(col("lng").cast(DataTypes.DoubleType), 2))
                .filter(year(col("wthr_date")).equalTo(lit(2022)));

        enrichedDf = receiptsDf.join(weatherDf, receiptsDf.col("date").equalTo(weatherDf.col("date"))
                .and(receiptsDf.col("lat").equalTo(weatherDf.col("lat")))
                .and(receiptsDf.col("lng").equalTo(weatherDf.col("lng"))), "inner");
    }

    @Test
    public void testReceiptDataIsNotNull() {
        assertNotNull("Receipts dataframe should not be null", receiptsDf);
    }

    @Test
    public void testWeatherDataIsNotNull() {
        assertNotNull("Weather dataframe should not be null", weatherDf);
    }

    @Test
    public void testEnrichedDataIsNotNull() {
        assertNotNull("Enriched dataframe should not be null", enrichedDf);
    }

    @Test
    public void testEnrichedDataFrameIsNotEmpty() {
        assertTrue("Enriched dataframe should not be empty", enrichedDf.count() > 0);
    }
    /**
     * Checks that all expected columns are present in the enriched DataFrame.
     */
    @Test
    public void testExpectedColumnsPresent() {
        String[] expectedColumns = {"date", "lat", "lng", "avg_tmpr_c", "total_cost", "discount"};
        List<String> columnList = Arrays.asList(enrichedDf.columns());
        for (String col : expectedColumns) {
            assertTrue("Expected column missing: " + col, columnList.contains(col));
        }
    }

    /**
     * Ensures that the filter for year being equal to 2022 is working as intended.
     */
    @Test
    public void testYearFilter() {
        assertEquals("All records should be from the year 2022",
                2022,
                enrichedDf.select(year(col("date")).alias("year")).distinct().head().getInt(0));
    }

    /**
     * Tests if the data join on date and location (latitude, longitude) keys are correct.
     */
    @Test
    public void testDataJoinCorrectness() {
        assertEquals("Joined data should match on keys",
                0,
                enrichedDf.filter(col("date").isNotNull()
                        .and(col("lat").isNotNull())
                        .and(col("lng").isNotNull())).count() - enrichedDf.count());
    }

    /**
     * Validates that the filtering based on the average temperature greater than 0 degrees works as expected.
     */
    @Test
    public void testTemperatureFilter() {
        assertEquals("All records should have avg_tmpr_c > 0", 0, enrichedDf.filter(col("avg_tmpr_c").leq(0)).count());
    }
}


