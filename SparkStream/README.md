
# DataEnrichment Application

The DataEnrichment application enriches receipt data from restaurants with weather data, computes various aggregations, and identifies the most popular order types for each franchise by date. The application outputs aggregated data to a specified location.

## System Requirements

- Java 8 or higher
- Apache Spark 3.x

## Installation

1. **Install Java:**
   Ensure Java JDK 8 or higher is installed on your system. You can check your Java version by running:
   ```bash
   java -version
   ```

2. **Install Apache Spark:**
   - Download Apache Spark from the official [Apache Spark website](https://spark.apache.org/downloads.html).
   - Unzip the downloaded file into a directory of your choice.
   - Add the `bin` directory of the unzipped Spark to the PATH environment variable.
   - Set the `SPARK_HOME` environment variable to the directory where Spark is installed.

## Setup

1. **Data Preparation:**
   - Place your `receipts_restaurants.csv` and `weather.csv` files into the `src/main/resources/` directory.
   - Ensure that the CSV files conform to the expected schema mentioned in the code documentation.

2. **Build the Project:**
   - Use the following command to compile the project (ensure you are in the project directory):
     ```bash
     mvn clean package
     ```

3. **Running the Application:**
   - Once built, you can run your Spark application using:
     ```bash
     spark-submit --class org.example.DataEnrichment target/data-enrichment-1.0-SNAPSHOT.jar
     ```

## Application Workflow

1. **Reading Data:**
   - Reads the 2022 data from the `receipts_restaurants.csv` and `weather.csv` files, preprocessing fields like dates and coordinates.

2. **Data Enrichment:**
   - Joins receipts with weather data on matching dates and coordinates.

3. **Data Filtering and Computation:**
   - Filters data based on temperature.
   - Calculates real total costs and classifies order sizes.

4. **Aggregation:**
   - Aggregates data to find the most popular order type per franchise per date.

5. **Output:**
   - Writes the result to the specified output path in CSV format.

## Directory Structure

- `src/`: Contains source code.
   - `main/`: Main application source files.
   - `test/`: Test files.
- `target/`: Contains compiled code and artifacts.

## Additional Features

### Spark Streaming Integration

To process streaming data:

1. Prepare a set of CSV files containing receipt and weather data.
2. Use Spark Streaming to read data from an input directory continuously.
3. Apply transformations and write the output to a storage system in real-time.

### Running Spark Streaming

- Set up the input and output directories for streaming data.
- Ensure Spark is configured to handle streaming data.
- Run the streaming job using:
  ```bash
  spark-submit --class org.example.DataEnrichmentStreaming target/data-enrichment-1.0-SNAPSHOT.jar
  ```

## Troubleshooting

- **Java Version Issues:** Make sure JAVA_HOME is set to a JDK version compatible with your Spark version.
- **Spark Errors:** Check the Spark logs for any errors related to memory or configuration.

## Conclusion

The DataEnrichment application demonstrates a practical implementation of big data processing using Apache Spark, enhancing data with additional insights that can support business decisions. Follow the setup instructions carefully to ensure a successful execution of the application.


